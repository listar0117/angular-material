import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router'
import { CustomerService } from '../../../services/customer.service';

export interface Transaction {
  kaek: string;
  ChargeDate: string;
  amount:number;
  
}

@Component({
  selector: 'customer-view-charge',
  templateUrl: './customer-view-charge.component.html',
  styleUrls: ['./customer-view-charge.component.scss']
})

export class CustomerViewChargeComponent implements OnInit {
  afm:string;
  [x: string]: any;
  kaek: string;
  ChargeDate : Date;
  amount: number;
  charges = [];
  loading = true;
  displayedColumns: string[] = ['kaek', 'ChargeDate', 'amount'];

  constructor(
    private formBuilder: FormBuilder,
    private customerService: CustomerService,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute){}
    
  ngOnInit() {
    this.afm = this.route.snapshot.paramMap.get('afm');
    this.charges = [];
    this.getcharges(0);
  }

  getcharges(index) {
    this.customerService.getCharges(this.afm).subscribe((res: any) => {
      this.charges = res.charges;
     this.loading = false;
    });
    if (index < 5) {
      this.CustomerService.getCharges().subscribe((res: any) => {
        this.charges = res.charges;
        return;
      },
        (err) => {
          if (err.error.code === 401 && err.error.message === 'Invalid JWT') {
            this.router.navigate(['/', 'login']);
          } else {
            this.getCustomers(index + 1);
          }

        }
      );
    } else {
      this.loading = false;
      return;
    }
  }

  /** Gets the total cost of all transactions. */
  getTotalCost() {
      return this.transactions.map(t => t.amount).reduce((acc, value) => acc + value, 0);
  }
    

    addColumn() {
      alert(this.afm);
      this.router.navigate([`/pages/customer/${this.afm}/fixedfee`]);
    }
}



