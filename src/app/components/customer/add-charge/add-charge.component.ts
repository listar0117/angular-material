import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { CustomerService } from '../../../services/customer.service';


@Component({
  selector: 'add-charge',
  templateUrl: './add-charge.component.html',
  styleUrls: ['./add-charge.component.scss']
})
export class CustomerAddFixedComponent implements OnInit {
  [x: string]: any;
  saveForm: FormGroup;
  saving = false;
  afm:string;
  loading =false ;
  

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private customerService: CustomerService,
    private route: ActivatedRoute,
    private toastr: ToastrService) {}
    
  ngOnInit() {   
    this.saveForm = this.formBuilder.group({
      kaek: ['', Validators.required],
      date: ['', Validators.required],
      amount: ['',Validators.required]
    });
    this.afm = this.route.snapshot.paramMap.get('afm');
  }      
   
  get f() { return this.saveForm.controls; }

 
  onSave() {
    if (this.saveForm.invalid) {
      return;
    }
    if (!this.saving) {
      const formValues = this.saveForm.value;
      formValues.ibans = [formValues.ibans];                           
      this.saveFunc(formValues, 0);
    
    }
  }

  saveFunc(data: any, index: number) {
    if (index < 5) {
      this.customerService.saveCharge(this.saveForm.value).subscribe((res: any) => {
        this.saving = false;
        this.toastr.success("New Charge  has been saved successfully.")
        return;
      },
        (err) => {
          this.toastr.error("Error while saving")
          this.saveFunc(data, index + 1);
        }
      )
    } else {
      this.saving = false;
      this.toastr.error("Cannot save this charge.")
      return;
    }
    
  }
}
